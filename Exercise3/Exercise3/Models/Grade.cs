﻿using System.ComponentModel.DataAnnotations;

namespace Exercise3.Models
{
    public class Grade
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Num { get; set; }
    }
}