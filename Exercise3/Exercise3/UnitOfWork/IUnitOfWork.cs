﻿using System;
using Exercise3.Models;
using Exercise3.Repository.GenericRepository;

namespace Exercise3.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Grade> GradeRepository { get;}
        IGenericRepository<Student> StudenRepository { get;}
        void Save();
    }
}