﻿using System;
using Exercise3.Models;
using Exercise3.Repository.GenericRepository;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using Microsoft.CodeAnalysis;

namespace Exercise3.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private SchoolContext _context;

        public UnitOfWork(SchoolContext context)
        {
            _context = context;
            InitReo();
        }
        
        private bool _disposed  = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public IGenericRepository<Grade> GradeRepository { get; set; }
        public IGenericRepository<Student> StudenRepository { get; set; }

        private  void InitReo()
        {
            GradeRepository = new GenericRepository<Grade>(_context);
            StudenRepository = new GenericRepository<Student>(_context);
            
        } 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

       
    }
}