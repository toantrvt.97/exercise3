﻿using Exercise3.Models;
using Exercise3.Service.StudentService;
using Exercise3.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace Exercise3.Controllers
{
    public class StudentController : Controller
    {
        private IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_studentService.GetAll());
        }
        // GET
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Student student)
        {
            _studentService.Create(student);
            _studentService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var getId = _studentService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Update(Student student)
        {
            _studentService.Update(student);
            _studentService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _studentService.Delete(id);
            _studentService.Save();
            return RedirectToAction("Index");
        }
    }
}