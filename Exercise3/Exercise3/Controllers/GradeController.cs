﻿using Exercise3.Models;
using Exercise3.Service.GradeService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;

namespace Exercise3.Controllers
{
    public class GradeController : Controller
    {
        private readonly IGradeService _gradeService;

        public GradeController(IGradeService gradeService)
        {
            _gradeService = gradeService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return View(_gradeService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Grade grade)
        {
            _gradeService.Create(grade);
            _gradeService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var getId = _gradeService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Update(Grade grade)
        {
            _gradeService.Update(grade);
            _gradeService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _gradeService.Delete(id);
            _gradeService.Save();
            return RedirectToAction("Index");
        }
    }
}