﻿using System.Collections.Generic;

namespace Exercise3.Repository.GenericRepository
{
    public interface IGenericRepository<TEntity>
    {
        TEntity GetById(int? id);
        IEnumerable<TEntity> GetAll();
        void Create(TEntity entity);
        void Delete(int id);
        void Update(TEntity entity);
        void Save();

    }
}