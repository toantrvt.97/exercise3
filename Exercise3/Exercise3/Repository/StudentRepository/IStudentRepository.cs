﻿using Exercise3.Models;
using Exercise3.Repository.GenericRepository;

namespace Exercise3.Repository.StudentRepository
{
    public interface IStudentRepository : IGenericRepository<Student>
    {
        
    }
}