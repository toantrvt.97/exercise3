﻿using Exercise3.Models;
using Exercise3.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Exercise3.Repository.StudentRepository
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        protected StudentRepository(SchoolContext context) : base(context)
        {
        }
    }
}