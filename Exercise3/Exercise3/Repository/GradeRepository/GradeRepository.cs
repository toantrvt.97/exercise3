﻿using Exercise3.Models;
using Exercise3.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Exercise3.Repository.GradeRepository
{
    public class GradeRepository : GenericRepository<Grade>, IGradeRepository
    {
        public GradeRepository(SchoolContext context) : base(context)
        {
            
        }
    }
}