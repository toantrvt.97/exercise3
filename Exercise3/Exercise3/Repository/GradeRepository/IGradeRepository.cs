﻿using Exercise3.Models;
using Exercise3.Repository.GenericRepository;

namespace Exercise3.Repository.GradeRepository
{
    public interface IGradeRepository : IGenericRepository<Grade>
    {
        
    }
}