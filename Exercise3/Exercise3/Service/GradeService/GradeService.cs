﻿using System.Collections.Generic;
using Exercise3.Models;
using Exercise3.UnitOfWork;

namespace Exercise3.Service.GradeService
{
    public class GradeService : IGradeService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GradeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Grade GetById(int? id)
        {
           return _unitOfWork.GradeRepository.GetById(id);
        }

        public IEnumerable<Grade> GetAll()
        {
            return _unitOfWork.GradeRepository.GetAll();
        }

        public void Create(Grade grade)
        {
            _unitOfWork.GradeRepository.Create(grade);
        }

        public void Delete(int id)
        {
            _unitOfWork.GradeRepository.Delete(id);
        }

        public void Update(Grade grade)
        {
            _unitOfWork.GradeRepository.Update(grade);
        }

        public void Save()
        {
            _unitOfWork.GradeRepository.Save();
        }
    }
}