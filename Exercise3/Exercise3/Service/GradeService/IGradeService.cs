﻿using System.Collections.Generic;
using System.Security.Permissions;
using Exercise3.Models;
using Exercise3.UnitOfWork;

namespace Exercise3.Service.GradeService
{
    public interface IGradeService
    {
        Grade GetById(int? id);
        IEnumerable<Grade> GetAll();
        void Create(Grade grade);
        void Delete(int id);
        void Update(Grade grade);
        void Save();
    }
}