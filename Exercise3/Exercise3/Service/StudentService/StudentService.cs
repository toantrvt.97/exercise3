﻿using System.Collections.Generic;
using Exercise3.Models;
using Exercise3.UnitOfWork;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Internal;

namespace Exercise3.Service.StudentService
{
    public class StudentService : IStudentService
    {
        private IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Student GetById(int? id)
        {
            return _unitOfWork.StudenRepository.GetById(id);
        }

        public IEnumerable<Student> GetAll()
        {
            return _unitOfWork.StudenRepository.GetAll();
        }

        public void Create(Student student)
        {
            _unitOfWork.StudenRepository.Create(student);
        }

        public void Delete(int id)
        {
            _unitOfWork.StudenRepository.Delete(id);
        }

        public void Update(Student student)
        {
            _unitOfWork.StudenRepository.Update(student);
        }

        public void Save()
        {
            _unitOfWork.StudenRepository.Save();
        }
    }
}