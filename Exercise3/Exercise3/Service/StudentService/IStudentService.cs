﻿using System.Collections.Generic;
using Exercise3.Models;

namespace Exercise3.Service.StudentService
{
    public interface IStudentService
    {
        Student GetById(int? id);
        IEnumerable<Student> GetAll();
        void Create(Student student);
        void Delete(int id);
        void Update(Student student);
        void Save();
    }
}